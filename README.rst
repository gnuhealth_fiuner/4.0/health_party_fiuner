.. image:: https://www.gnuhealth.org/downloads/artwork/logos/isologo-gnu-health.png

GNU Health HMIS: Libre Hospital Management and Health Information System
========================================================================

GNU Health party FIUNER module
==================================
Este modulo 
 * agrega un modelo llamado Partido y un campo referido a tal modelo en la tabla de
 direcciones de terceros.
 
 
 
This module: 
 * adds a model named Partido and a field name partido referenced to that model on the
party addresses


Documentation
-------------
*GNU Health
Wikibooks: https://en.wikibooks.org/wiki/GNU_Health/

Contact
-------
* GNU Health Contact 
 - website: https://www.gnuhealth.org
 - email: info@gnuhealth.org
 - Twitter: @gnuhealth

* FIUNER Contact 
 - email: saludpublica@ingenieria.uner.edu.ar

License
--------

GNU Health is licensed under GPL v3+::

 Copyright (C) 2008-2021 Luis Falcon <falcon@gnuhealth.org>
 Copyright (C) 2011-2021 GNU Solidario <health@gnusolidario.org>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.


Prerequisites
-------------

 * Python 3.4 or later (http://www.python.org/)
 * Tryton 5.0 (http://www.tryton.org/)


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
