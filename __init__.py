#This file is part health_party_fiuner module for Tryton.
#The COPYRIGHT file at the top level of this repository contains
#the full copyright notices and license terms.

from trytond.pool import Pool
from . import health_party
from . import address

def register():
    Pool.register(
        health_party.Party,
        address.Address,
        address.Partido,
        module='health_party_fiuner', type_='model')
