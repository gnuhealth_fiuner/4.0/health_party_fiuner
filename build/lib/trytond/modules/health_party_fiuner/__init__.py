#This file is part health_party_fiuner module for Tryton.
#The COPYRIGHT file at the top level of this repository contains
#the full copyright notices and license terms.

from trytond.pool import Pool
from .health_party import *
from .address import *

def register():
    Pool.register(
        Party,
        Address,
        Partido,
        module='health_party_fiuner', type_='model')
